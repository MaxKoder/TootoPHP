<?php

require 'tootophp\autoload.php';

// Setting up your instance
$tootoPHP = new TootoPHP\TootoPHP('mastodon.xyz');

// Setting up your App name and your website
$app = $tootoPHP->registerApp('TootoPHP', 'http://max-koder.fr');
if ( $app === false) {
    throw new Exception('Problem during register app');
}

echo $app->getAuthUrl();

// Copy this URL and open it.
// Allow App to use Mastodon and copy the given token